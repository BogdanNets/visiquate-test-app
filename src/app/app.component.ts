import { Component } from '@angular/core';
import { trigger, transition } from '@angular/animations';

import { AnimationConstants } from './shared/constants/animation.constants';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    trigger("animRoutes", [
        transition(":increment", AnimationConstants.slideTo("right")),
        transition(":decrement", AnimationConstants.slideTo("left")),
    ]),
]
})
export class AppComponent {
  title = 'test-app-visiQuate';
}
