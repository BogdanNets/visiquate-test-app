import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import { UserDataService } from 'src/app/shared/services/user-data.service';
import { UserListModel } from 'src/app/shared/models/user';
import { Router } from '@angular/router';

@Component({
    selector: 'app-user-list',
    templateUrl: './user-list.component.html',
    styleUrls: ['./user-list.component.scss'],
    providers: [UserDataService]
})

export class UserListComponent implements OnInit {
    dataSource: MatTableDataSource<UserListModel>;
    displayedColumns: string[] = ['id', 'avatar_url', 'name', 'profile_url'];

    @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
    @ViewChild(MatSort, {static: true}) sort: MatSort;

    constructor(
        private dataService: UserDataService,
        private router: Router
    ) { }

    ngOnInit(): void {
        this.getUsers();
    }

    getUsers(): void {
        this.dataService.getUsers()
            .subscribe((data: UserListModel[]) => {
                this.dataSource = new MatTableDataSource<UserListModel>(data);
                this.dataSource.paginator = this.paginator;
            }
        );
    }

    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.dataSource.filter = filterValue.trim().toLowerCase();
    
        if (this.dataSource.paginator) {
          this.dataSource.paginator.firstPage();
        }
    }

    getUserId(id: number) {
        this.router.navigate(['/userInfo'], { queryParams: { user: id } });
    }

}
