import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { UserDataService } from 'src/app/shared/services/user-data.service';
import { UserInfoModel } from 'src/app/shared/models/userInfo';
import { SharedService } from 'src/app/shared/services/shared.service';

@Component({
    selector: 'app-user-info',
    templateUrl: './user-info.component.html',
    styleUrls: ['./user-info.component.scss'],
    providers: [UserDataService]
})

export class UserInfoComponent implements OnInit {
    user: string;
    userInfo: UserInfoModel;

    constructor(
    private _dataService: UserDataService,
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private sharedService: SharedService
    ) { }

    ngOnInit(): void {
        this._activatedRoute.queryParams.subscribe(data => this.user = data.user);
        this.getUserInfo(this.user);
    }

    getUserInfo(user: string): void {
        this._dataService.getUserInfo(user)
            .subscribe((user: UserInfoModel) => {
                this.newMessage(user.repos_url);
                return this.userInfo = user
            } 
        )
    }

    goToRepositoryLinks() {
        this._router.navigate(['/repoListView']);
    }

    newMessage(url: string) {
        this.sharedService.shareRepLink(url);
    }

}
