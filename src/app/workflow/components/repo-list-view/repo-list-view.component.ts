import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';


import { UserDataService } from 'src/app/shared/services/user-data.service';
import { UserRepositoryModel } from 'src/app/shared/models/repository';
import { SharedService } from 'src/app/shared/services/shared.service';

@Component({
    selector: 'app-repo-list-view',
    templateUrl: './repo-list-view.component.html',
    styleUrls: ['./repo-list-view.component.scss']
})
export class RepoListViewComponent implements OnInit {
    repositoriesLink: string;
    repoList: UserRepositoryModel;
    dataSource: MatTableDataSource<UserRepositoryModel>;
    displayedColumns: string[] = ['name', 'description', 'url', 'forks_count', 'watchers_count'];

    @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

    constructor(
        private sharedService: SharedService,
        private _dataService: UserDataService) { }

    ngOnInit(): void {
        this.sharedService.sharedLink.subscribe(link => {
            if (link) {
                localStorage.setItem('repositoriesLink', link);
            }
             console.log(link)
            this.repositoriesLink = link;
            return this.repositoriesLink;
        });

        this.getRepositories(localStorage.getItem('repositoriesLink'));
    }

    getRepositories(url: string): void {
        this._dataService.getRepositories(url)
            .subscribe((data: UserRepositoryModel[]) => {
                this.dataSource = new MatTableDataSource<UserRepositoryModel>(data);
                this.dataSource.paginator = this.paginator;
            })
    }

}
