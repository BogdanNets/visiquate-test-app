import { NgModule } from "@angular/core";

import { MaterialModule } from "./material.module";

@NgModule({
    imports: [
        MaterialModule,
    ],
    declarations: [
    ],
    exports: [
        MaterialModule,
    ],
    entryComponents: [
    ],
})

export class SharedModule { }
