import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { UserListModel } from '../models/user';
import { UserInfoModel } from '../models/userInfo';
import { UserRepositoryModel } from '../models/repository';

@Injectable({
    providedIn: 'root'
})
export class UserDataService {
    private _usersRequestURL: string = 'https://api.github.com/users';

    constructor(protected readonly http: HttpClient) {}
    
    getUsers(): Observable<UserListModel[]> {
        return this.http.get<UserListModel[]>(this._usersRequestURL);
    }

    getUserInfo(user: string): Observable<UserInfoModel> {
        return this.http.get<UserInfoModel>(`${this._usersRequestURL}/${user}`);
    }

    getRepositories(link: string): Observable<UserRepositoryModel[]> {
        return this.http.get<UserRepositoryModel[]>(link);
    }
  
}
