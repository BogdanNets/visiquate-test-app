import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class SharedService {

    private link = new BehaviorSubject('');
    sharedLink = this.link.asObservable();

    constructor() { }

    shareRepLink(link: string) {
        this.link.next(link);
    }
  
}
