import { query, group, style, animate, AnimationMetadata } from "@angular/animations";

export class AnimationConstants {
    private static _timingSlider: string = ".8s ease-in-out";
    private static _optional: object = { optional: true };

    static slideTo(direction: string): Array<AnimationMetadata> {
        return [
            query(":enter, :leave", style({
                position: "absolute", top: 0,
                [direction]: 0, "background-color": "white",
                width: "100%", border: "{{ border }}",
                "box-shadow": "{{ shadow }}"
            }), AnimationConstants._optional),
            query(":enter", style({ [direction]: "-100%" }), AnimationConstants._optional),
            group([
                query(":leave", [animate(AnimationConstants._timingSlider, style({ [direction]: "200%", opacity: 0 }))], AnimationConstants._optional),
                query(":enter", [animate(AnimationConstants._timingSlider, style({ [direction]: "-0%", opacity: 1 }))], AnimationConstants._optional),
            ]),
        ];
    }
}
