export class UserInfoModel {
    public name?: string;
    public company?: string;
    public email?: string;
    public followers?: string;
    public updated_at?: string;
    public avatar_url?: string;
    public repos_url?: string;
}