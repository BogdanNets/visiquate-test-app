export class UserListModel {
    public id?: string;
    public avatar_url?: string;
    public login?: string;
    public profile_url?: string;
}