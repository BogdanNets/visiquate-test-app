export class UserRepositoryModel {
    public name?: string;
    public description?: string;
    public url?: string;
    public forks_count?: string;
    public watchers_count?: string;
}