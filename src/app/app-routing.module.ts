import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RepoListViewComponent, UserInfoComponent, UserListComponent } from './workflow';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'userList' },
  { path: 'repoListView', component: RepoListViewComponent},
  { path: 'userInfo', component: UserInfoComponent},
  { path: 'userList', component: UserListComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
